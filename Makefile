FC = gfortran
CFLAG =  -Wall -pedantic -std=f95 -O0 -fopenmp -Wmaybe-uninitialized -fall-intrinsics -g -ffpe-trap=zero,invalid,overflow,underflow
#CFLAG =  -Wall -pedantic -std=f95 -O3 -Wmaybe-uninitialized
#LFLAG = -llapack -lblas -lfftw3 -lm 

.SUFFIXES : .o .F90 .f .f90
.f90.o:
	$(FC) $(CFLAG) -c $<
.F90.o:
	$(FC) $(CFLAG) -c $<
.f.o:
	$(FC) $(CFLAG) -c $<

%.o: %.mod

MAIN = main.o

MODS = \
kinds.o \
variables.o \
tdpes.o \

all:  main.x

main.x: $(MAIN) $(MODS) $(SUBS)
	$(FC) $(CFLAG) -o $@ $(MAIN) $(MODS) $(SUBS) \
	$(JDLIB) $(WSMPLIB) $(LFLAG)

.PHONY: clean
clean:
	rm -f *.o *.mod core fort.* *~ *.x

include make.depends
