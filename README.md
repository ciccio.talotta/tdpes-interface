# Exact TDPES Interface

This Fortran 90 interface reads the Time Dependent Potential Energy Surface (TDPES) as a text file. TDPES data is retrivied from an exact calculation
performed with the ElVibRot code. The interface provides the energy `ipot` and gradient `igrad` as output, for a set positions `Ri`. The interface performs a
weighted interplation between two TDPES grid points, so `Ri` can be any point in between the TDPES grid limits.

## Usage

The subroutine needs four parameters as input:

1. `path` (CHARACTER): where to find the TDPES ASCII files.
2. `timestep` (INTEGER): the timestep according to the TDPES parameters, for instance `timestep='./TULLY1/k25/40fs-0.1fs/TDPES/'` (see next section)
3. `ntraj` (INTEGER): the number of trajectories
4. `Ri`(REAL): an array of size `ntraj` with the position of each trajectory.

It gives as output:

1. `ipot` an array of size `ntraj` with energy for each `Ri` position
2. `igrad` an array of size `ntraj` with the gradient for each `Ri` position

To use the subroutine in your Fortran program:

```fortran
CALL exact_tdpes(path, timestep, ntraj, Ri, ipot, igrad)
```

## TDPES Files

TDPES ASCII files are grouped according to the initial momentum k, and the time step in fs used to propagate the exact wavepacket through ElVibRot program.

The repository contains two .tar.gz files: `TULLY1.tar.gz`, `TULLY2.tar.gz` and `TULLY3.tar.gz`.

Each `TULLY` foolder contains subfolders such as: `k10`, `k15`, `k20`, `k25`. Each subfolder
provides TDPESs for a particular initial momentum (k) value, in atomic units. For more information about the parameters please refer to the tables in the `TDPES_Tully1_Tully2_Tully3.pdf` file. 

TDPES ASCII data is written into the `EpsilonNNNN.dat` files, where NNNN corresponds to the time step with leading zeros.  
