MODULE tdpes

  USE kinds
  USE variables
  IMPLICIT NONE

  CONTAINS

  SUBROUTINE exact_tdpes(path, timestep, ntraj, Ri, ipot, igrad) 

  INTEGER,            INTENT(IN) :: timestep, ntraj
  REAL(KIND=dp),      INTENT(IN) :: Ri(ntraj) 
  CHARACTER(LEN=200), INTENT(IN) :: path

  REAL(KIND=dp), INTENT(OUT)     :: ipot(ntraj), igrad(ntraj)

  path_to_input=path

  CALL read_grid() 
  CALL read_potential(timestep)

  CALL en_grad_lin_int(Ri, ntraj, ipot, igrad) 

  END SUBROUTINE exact_tdpes 

!***********Read the grid points from the first TDPES File and allocate IN and OUT arrays************!
  SUBROUTINE read_grid() 

  INTEGER                       :: ios,ix
  CHARACTER(LEN=200)            :: filename,comment

  filename=TRIM(path_to_input)//"Epsilon001.dat"

  OPEN(100,FILE=TRIM(filename),ACTION="read",STATUS="old",FORM="formatted",IOSTAT=ios)
      IF(ios/=0) PRINT*,'ERROR OPENING ',TRIM(filename)

      READ(100,*) comment, dofs
      READ(100,*) comment, points

      ALLOCATE(grid(points),potential(points))

      DO ix=1,points
        READ(100,*) grid(ix)
      ENDDO

  CLOSE(100)

  delta=(grid(points)-grid(1))/DBLE(points)

  END SUBROUTINE read_grid 



!***********Read the TDPES potential for each time step************!
  SUBROUTINE read_potential(timestep)

    INTEGER, INTENT(IN)           :: timestep
 
    CHARACTER(LEN=200)            :: idx_step,filename,comment_line,tmp
    INTEGER                       :: ios,ix

    WRITE(idx_step,"(I3.3)") timestep

    potential=0.0_dp

    filename=TRIM(path_to_input)//'Epsilon'//TRIM(idx_step)//".dat"

    OPEN(100,FILE=TRIM(filename),ACTION="read",STATUS="old",FORM="formatted",IOSTAT=ios)
      IF(ios/=0) PRINT*,'ERROR OPENING ',TRIM(filename)

      READ(100,*) comment_line
      READ(100,*) comment_line

      DO ix=1,points
        READ(100,*) tmp, potential(ix)
      ENDDO

    CLOSE(100)

  END SUBROUTINE read_potential



!***********Interpolate for each time step************!
  SUBROUTINE en_grad_lin_int(Ri, ntraj, ipot, igrad) 

   INTEGER,      INTENT(IN)  :: ntraj
   REAL(KIND=dp),INTENT(IN)  :: Ri(ntraj)
   REAL(KIND=dp),INTENT(OUT) :: ipot(ntraj),igrad(ntraj)

   INTEGER       :: igrid_max, igrid_min, ix, itraj
   REAL(KIND=dp) :: distance(3), weight(2), grad(2), c1, c2

   ipot=0.0d0
   igrad=0.0d0

   c1=1.0_dp/12.0_dp
   c2=8.0_dp/12.0_dp

! Loop over the trajectories to get potentials and gradients 
   DO itraj=1,ntraj   

      igrid_max=0
      igrid_min=0

      distance=0
      weight=0
      grad=0
   
   ! Check if the point is inside the grid boundaries
      IF(Ri(itraj).GE.grid(1).AND.Ri(itraj).LE.grid(points)) THEN
   
        DO ix=1,points
         IF (grid(ix).GT.Ri(itraj)) THEN
          igrid_max=ix
          EXIT
         ENDIF 
        ENDDO
       
        igrid_min=igrid_max-1 
   
        distance(1)=ABS(Ri(itraj)-grid(igrid_min))
        distance(2)=ABS(Ri(itraj)-grid(igrid_max))
        distance(3)=ABS(grid(igrid_max)-grid(igrid_min))
   
        weight(1)=1.0d0-distance(1)/distance(3)
        weight(2)=1.0d0-distance(2)/distance(3)
   
        ipot(itraj)=(potential(igrid_min)*weight(1)+potential(igrid_max)*weight(2))/(weight(1)+weight(2))
   
        IF(igrid_min==1) THEN
         grad(1)=(-3.d0*potential(1)+4.d0*potential(2)-potential(3))/(2.d0*delta)                                                ! + * * * * * * *
         grad(2)=(potential(3)-potential(1))/(2.d0*delta)                                                                        ! * + * * * * * * 
        ELSEIF (igrid_min==2) THEN 
         grad(1)=(potential(3)-potential(1))/(2.d0*delta)                                                                        ! * + * * * * * * 
         grad(2)=-(c1*(potential(5)-potential(1))+c2*(potential(2)-potential(4)))/delta                                          ! * * + * * * * * 
        ELSEIF (igrid_max==points) THEN 
         grad(1)=(potential(igrid_min+1)-potential(igrid_min-1))/(2.d0*delta)                                                    ! * * * * * * + * 
         grad(2)=(+3.d0*potential(igrid_max)-4.d0*potential(igrid_max-1)+potential(igrid_max-2))/(2.d0*delta)                    ! * * * * * * * + 
        ELSEIF (igrid_max==points-1) THEN                                                                                    
         grad(1)=-(c1*(potential(igrid_min+2)-potential(igrid_min-2))+c2*(potential(igrid_min-1)-potential(igrid_min+1)))/delta  ! * * * * * + * * 
         grad(2)=(potential(igrid_max+1)-potential(igrid_max-1))/(2.d0*delta)                                                    ! * * * * * * + * 
        ELSE                                                                                                                  
         grad(1)=-(c1*(potential(igrid_min+2)-potential(igrid_min-2))+c2*(potential(igrid_min-1)-potential(igrid_min+1)))/delta  ! * * * + * * * * 
         grad(2)=-(c1*(potential(igrid_max+2)-potential(igrid_max-2))+c2*(potential(igrid_max-1)-potential(igrid_max+1)))/delta  ! * * * * + * * * 
        ENDIF
          
      igrad(itraj)=(grad(1)*weight(1)+grad(2)*weight(2))/(weight(1)+weight(2))
   
      !WRITE(*,*) ipot, igrid_min, igrid_max, potential(igrid_max), potential(igrid_min), igrad 
   
      ELSE 
        WRITE(*,*) 'Error, Ri outside grid limits'
        STOP
      ENDIF
  ENDDO

  END SUBROUTINE en_grad_lin_int

END MODULE tdpes
