PROGRAM TEST_PES

! For one dimension only (Tully models)

 USE tdpes 
 USE variables

 INTEGER timestep, ntraj, i

 REAL(kind=dp),ALLOCATABLE :: Ri(:), ipot(:), igrad(:)
 CHARACTER(LEN=200) :: path 

 path='./TULLY1/k10/40fs-0.1fs/TDPES/'

 READ(*,*) timestep, ntraj

 ALLOCATE(Ri(ntraj), ipot(ntraj), igrad(ntraj))

!Debug (create ntraj fake positions)
 DO i=1,ntraj
  Ri(i)=-14.d0+DBLE(i)*0.054
 ENDDO

 CALL exact_tdpes(path, timestep, ntraj, Ri, ipot, igrad)

 DO i=1,ntraj 
  WRITE(100,*) Ri(i), ipot(i), igrad(i)
 ENDDO

END 
