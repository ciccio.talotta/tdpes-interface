MODULE variables
  USE kinds

  IMPLICIT NONE
 
  REAL(KIND=dp),PARAMETER :: zero=0.00000000010_dp
  COMPLEX(KIND=dp)        :: Im_unit=(0.0_dp,1.0_dp)
  REAL(KIND=dp)           :: hbar=1.0_dp
  REAL(KIND=dp)           :: PI,delta

  REAL(KIND=dp), ALLOCATABLE :: grid(:), potential(:)                 !Input<<<<<

  INTEGER            :: points,dofs                                   !Internal Variables
  CHARACTER(LEN=200) :: path_to_input                                 !Internal Variables
  
END MODULE variables
